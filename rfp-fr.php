<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    
    <title>Appel à communications : Fearless Cities – Des villes sans peur</title>
</head>

<body>
	<?php require_once 'snippet.html'; ?>
	<div class="container-fluid">
		<div class="row mt-5 ml-3">
			<div class="col-sm pb-3 pr-0 pt-0 pl-3">
				<h1>Appel à communications</h1>
				<h2>Des villes sans peur Sommet Municipaliste Nord-Américain</h2>

			</div>
			<div class="col-sm pb-3 pr-3 pt-0 pl-0">
				<em>Fearless Cities, du 27 au 29 juillet à New York</em>, est le premier sommet municipaliste en Amérique du Nord. Coordonné par Movement Netlab, Cooperation Jackson et New York University en partenariat avec le le réseau municipaliste international Fearless Citices, ces trois jours rassembleront les acteurs des mouvements municipalistes du Canada, de Mexico et des Etats Unis pour explorer un large spectre d’expérimentation, électorales ou extra-électorales. « Win the City » - « Reprenons la ville » est notre slogan et le thème du sommet. Notre objectif est de renforcer las liens entre les mouvements qui se retrouvent dans cette initiative et tout ceux qui cherchent à améliorer les villes et villages pour tous. 

			</div>
		</div>
		<div class="row">
			<div class="col-md scheme-purple p-3">
				<div class="row p-3 pl-5">
						<!-- Begin Airtable embed -->
					<script src="https://static.airtable.com/js/embed/embed_snippet_v1.js"></script><iframe class="airtable-embed airtable-dynamic-height" src="https://airtable.com/embed/shrHVK7aO1NWoBiqv?backgroundColor=purple" frameborder="0" onmousewheel="" width="100%" height="3654" style="background: transparent; border: 1px solid #ccc;"></iframe>
				</div>

				<!--End airtable embed-->
			</div>
			<div class="col-md scheme-orange pl-3 pt-3 pr-3">
				<div class="row p-3 pr-sm-5">
					<p>
					Le municipalisme démocratise et féminise, de façon radicale, les institutions politiques au niveau le plus proche de nos vies de tous les jours : les mairies. A Fearless Cities, nous voulons :</p>
					<ul>
						<li>Galvaniser le réseau de mouvements municipalistes en plein essor</li>
						<li>Partager les succès et les reculs des mouvements à toutes leurs étapes de développement</li>
						<li>Ouvrir des possibilités concrètes pour tester de nouveaux outils de démocracie participative</li>
						<li>Faire converger les projets municipalistes au Canada, au Mexique et aux Etats-Unis</li>
					</ul>

					<p>Nous avons besoin de votre participation pour que ces rencontres soient collaboratives, inclusives et permettent d’échanger des informations. </p>
					<p>Applications are open now until <span class="text-purple">Friday, June 1st, 2018 at 11pm-EST</span>. Proposals will be evaluated on a rolling basis. All applicants will be notified of whether their proposal was accepted by no later than Friday, June 15th.</p>
					<p>We encourage you to submit using the online form. If you have specific urgent questions regarding the RFP please send an email to <a href="mailto:rfp@fearlesscities.nyc">rfp@fearlesscities.nyc</a></p>
					<p>Fearless Cities North America kickstarts a series of local municipalist gatherings across North America to build the ties for ongoing communication and collaboration. Submit your proposal today and join us to grow the municipalist movement!</p>

				</div>
				<div class="row p-3 pr-sm-5 scheme-yellow">
					<h2>Thematic Tracks</h2>
				
					<p><strong>Transforming Power</strong>: the visions, strategies, principles, and tactics that can build a more democratic, inclusive, and open city.</p>
					<ul><em>Possible topics:</em> social movements and electoral politics, democratizing institutions, community self-determination, centering equity, government transparency, Sanctuary Cities, bottom-up democracy, limitations of electoralism, building dual power, decolonization</ul>
					<p><strong>Tools for Participatory Democracy:</strong>  the methods and tools that are being used to deepen political participation in/outside of local governments including in-person and digital participation.</p>
					<ul><em>Possible topics:</em> crowdsourced policy platforms, participatory budgeting, civic technology, distributed governance, collective decision making, neighborhood assemblies, community-owned land trusts, resident-led platforms, cooperative economics, community-controlled resources.</ul>
					<p><strong>Queering / Feminizing Politics:</strong> the fundamental differences between the municipalist approach and traditional electoral campaigns: confluence, inclusion, gender parity, starting from the local level - the neighborhood and home.</p>
					<ul><em>Possible topics:</em> pragmatic steps to feminize politics, confluence and crowd sourced platforms, feminist social movements and municipal governance, inclusive leadership, familial relationships and migration, queering politics within the police state, what is equity?, reconfiguring the political landscape, leveraging participatory tech tools for feminization and inclusion.</ul>
					<p><strong>Municipal Concerns Today:</strong> issues that affect contemporary cities and towns today and the innovative ways local communities grapple with challenges and leverage opportunities.</p>
					<ul><em>Possible Topics:</em> housing, policing, climate change, gentrification, inequality, education, unequal development, urban-rural connections, preemption, sustainability.</ul>

				</div>
				<div class="row p-3 pr-sm-5 scheme-purple">
					<h2>Session Types</h2>
					<ul>
						<li><strong>Workshop:</strong> Participants practice a skill or set of skills with the support of a facilitator. A workshop requires appropriate materials, time and space for participants to practice or work together.</li>
						<li><strong>Strategy Session:</strong> Participants lay out a shared challenge or opportunity, identify shared affinities or needs, and try to come up with a solution, collaborative output, or methods of communication moving forward. A facilitator helps the participants walk through the process. Small group discussions and self-organizing can often bring out shared affinities, needs and strategy conversations. The solution is a plan of action which the participants commit to carry out after the session.</li>
						<li><strong>Open Lab</strong>: Participants join in skill-sharing, peer-learning, and hands-on collaboration, outcomes of the sessions are participant-driven. Open Labs are the only sessions that may be proposed either in advance, during the RFP process, or during the summit itself in order to provide space for emergent configurations.</li>
						<li><strong>Presentation / Panel:</strong> Participants receive information from one presenter or multiple panelists. The presentation may include media, activities, discussions, or Q&amp;A.</li>
					</ul>
					 
					<strong>For all sessions please clearly mark on your application any technical or material requirements.</strong>

				</div>
			</div>
		</div>
	</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>