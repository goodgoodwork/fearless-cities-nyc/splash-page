<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    
    <title>Ciudades sin Miedo - NA : Convocatoria de Propuestas</title>
</head>

<body>
	<?php require_once 'snippet.html'; ?>
	<div class="container-fluid">
		<div class="row mt-5 ml-3">
			<div class="col-sm pb-3 pr-0 pt-0 pl-3">
				<h1>Convocatoria de Propuestas</h1>
				<h2>Cumbre Municipalista Regional de América del Norte</h2>
				<h3>Applications are open now until Friday, June 1st 2018 at 11pm-EST. Applicants will be notified of approval no later than June 15th.</h3>



			</div>
			<div class="col-sm pb-3 pr-3 pt-0 pl-0">
				<em>Fearless Cities, del 27 al 29 de julio en Nueva York</em>, es la primera cumbre municipalista en América del Norte. Organizado por Movement Netlab, Cooperation Jackson y el Departamento de Estudios Metropolitanos de New York University en asociación con la red municipalista internacional de Fearless Cities, este encuentro de tres días reunirá a organizadores de movimientos municipales crecientes en Canadá, México y Estados Unidos para explorar la amplia gama de experimentos que tienen lugar dentro y fuera de las urnas. "Win the City" es el lema de este encuentro y el tema de la cumbre, y nuestro objetivo es construir la solidaridad entre activistas, organizadores y cualquier persona que busca mejorar las ciudades para todos.

			</div>
		</div>
		<div class="row">
			<div class="col-md scheme-purple p-3">
				<div class="row p-3 pl-5">
						<!-- Begin Airtable embed -->
					<script src="https://static.airtable.com/js/embed/embed_snippet_v1.js"></script><iframe class="airtable-embed airtable-dynamic-height" src="https://airtable.com/embed/shrHVK7aO1NWoBiqv?backgroundColor=purple" frameborder="0" onmousewheel="" width="100%" height="3654" style="background: transparent; border: 1px solid #ccc;"></iframe>
				</div>

				<!--End airtable embed-->
			</div>
			<div class="col-md scheme-orange pl-3 pt-3 pr-3">
				<div class="row p-3 pr-sm-5">
					<p>El municipalismo democratiza y feminiza radicalmente las instituciones políticas en el nivel más cercano a nuestra vida cotidiana, el gobierno municipal. En Fearless Cities, vamos a:</p>
					<ul>
						<li>Galvanizar una red creciente de movimientos municipales.</li>
						<li>Compartir los éxitos y los retrocesos de los movimientos en cada etapa del desarrollo.</li>
						<li>Proporcionar oportunidades prácticas para probar nuevas herramientas para la democracia participativa.</li>
						<li>Reunir proyectos municipales en Canadá, México y los Estados Unidos.</li>
					</ul>
					<p>Necesitamos su participación para crear una cumbre colaborativa, informativa e inclusiva. Envíe propuestas para talleres, sesiones de discusión estratégica, laboratorios abiertos o presentaciones / paneles utilizando el formulario en nuestra página web <a href="mailto:rfp@fearlesscities.nyc">fearlesscities.nyc</a>. Habrán estipendios limitados para cubrir gastos de transporte y alojamiento.</p>
					<p>
						El plazo de envío de propuestas está abierto hasta el <span class="text-purple">viernes 1 de junio de 2018 a las 11 de la noche -EST</span>. Las propuestas se evaluarán de forma continua desde el momento de su recepción. Se notificará a todos los solicitantes si su propuesta fue aceptada a más tardar el viernes 1 de junio de 2018 a las 11:00 de la noche -EST.
					</p>
					<p>Le recomendamos que envíe su propuesta usando el formulario en línea. Si tiene preguntas específicas urgentes sobre el RFP, envíe un correo electrónico a rfp@fearlesscities.nyc</p>
					<p>Fearless Cities North America inicia una serie de reuniones municipales locales en toda América del Norte para construir lazos para una comunicación y colaboración continua. <strong>¡Envíe su propuesta hoy y únase a nosotros para hacer crecer el movimiento municipalista!</strong></p>

				</div>
				<div class="row p-3 pr-sm-5 scheme-yellow">
					<h2>Ejes temáticos</h2>
					<p><strong>Transformación del Poder:</strong> las visiones, estrategias, principios y tácticas que pueden construir una ciudad más democrática, inclusiva y abierta.</p>
					<ul><em>Posibles temas:</em> movimientos sociales y política electoral, instituciones democratizadoras, autodeterminación comunitaria, equidad, transparencia gubernamental, Ciudades Santuario, democracia de abajo-arriba, limitaciones del electoralismo, construcción del poder dual, decolonización.</ul>
					<p><strong>Herramientas para la Democracia Participativa:</strong> los métodos y herramientas que se utilizan para profundizar la participación política dentro y fuera de los gobiernos locales, incluida la participación en persona y digital.</p>
					<ul><em>Posibles temas:</em> el “crowdsourcing” de plataformas políticas, presupuestos participativos, tecnología cívica, gobierno distribuido, toma de decisiones colectivas, asambleas vecinales, fideicomisos de tierras de propiedad comunitaria, plataformas dirigidas por residentes, economía cooperativa, recursos controlados por la comunidad.</ul>
					<p><strong>Feminización politica /Queerización politica :</strong> las diferencias fundamentales entre el enfoque municipalista y las campañas electorales tradicionales: confluencia, inclusión, paridad de género,  comenzando desde el nivel local: el vecindario y el hogar.</p>
					<ul><em>Posibles temas:</em> pasos pragmáticos para feminizar la política, confluencia y plataformas participativas, prácticas de crowdsourcing y crowdfunding, política del cuido, movimientos sociales feministas y gobernanza municipal, liderazgo inclusivo, relaciones familiares y migración, política homofóbica dentro del estado policial, ¿qué es la equidad?, reconfigurar el panorama político, aprovechar la tecnología participativa herramientas para la feminización y la inclusión.</ul>
					<p><strong>Preocupaciones municipales actuales:</strong> problemas que afectan a las ciudades y pueblos contemporáneos de hoy y las formas innovadoras en que las comunidades locales lidian con los desafíos y oportunidades de apalancamiento.</p>
					<ul><em>Posibles temas:</em> vivienda, vigilancia, cambio climático, gentrificación, desigualdad, educación, desarrollo desigual, conexiones urbano-rurales, apropiación, sostenibilidad.</ul>
				
					

				</div>
				<div class="row p-3 pr-sm-5 scheme-purple">
					<h2>Tipos de sesión</h2>
					<ul>
						<li><strong>Taller:</strong> los participantes practican una habilidad o conjunto de habilidades con el apoyo de un facilitador. Un taller requiere materiales apropiados, tiempo y espacio para que los participantes practiquen o trabajen juntos.</li>
						<li><strong>Sesión de estrategia:</strong> los participantes plantean un desafío u oportunidad compartida, identifican afinidades o necesidades compartidas e intentan encontrar una solución, un resultado colaborativo o métodos de comunicación en el futuro. Un facilitador ayuda a los participantes a recorrer el proceso. Las discusiones en grupos pequeños y la autoorganización a menudo pueden generar afinidades compartidas, necesidades y conversaciones estratégicas. La solución es un plan de acción que los participantes se comprometen a llevar a cabo después de la sesión.</li>
						<li><strong>Laboratorio abierto:</strong> los participantes se unen en el intercambio de habilidades, el aprendizaje entre iguales y la colaboración práctica, los resultados de las sesiones son impulsados ​​por los participantes. Los Open Labs son las únicas sesiones que pueden proponerse de antemano, durante el proceso de RFP o durante la cumbre para proporcionar espacio para las configuraciones emergentes.</li>
						<li><strong>Presentación / Panel:</strong> los participantes reciben información de un presentador o de varios panelistas. La presentación puede incluir medios, actividades, discusiones o preguntas y respuestas.</li>
					</ul>
					 
					<strong>Para todas las sesiones, marque claramente en su aplicación los requisitos técnicos o de material.</strong>

				</div>
			</div>
		</div>
	</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>